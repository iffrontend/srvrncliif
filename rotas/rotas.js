
function modificaCachePgProcessamento(data) {
  return new Promise(function(resolve, reject){
    data.novaProp = 'funcionou';
    if (!data) reject("Precisa informar algo válido.")

    let response = {};

    response.sentidoHorarioLiberado = true;
    response.sentidoAntiHorarioLiberado = true;
    response.usuarioNome = 'Fulano de tal';
    response.texto = 'texto 123';
    response.msgRecepcao = 'texto para recepcao';
    response.liberacaoTempo = 5000;
    response.semComando = false;
    response.nsLeitor = '123';
    response.nsPlc = '234';
    response.convidado = false;
    response.grupo = '1';
    response.nomeCatraca = 'Catraca1';
    
    
    resolve(response);
  })
}
module.exports = modificaCachePgProcessamento;