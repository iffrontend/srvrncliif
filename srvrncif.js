var proxy = require('express-http-proxy');
var app = require('express')();

var modificaCachePgProcessamento = require("./rotas/rotas.js")


app.use('/metodos/consultaUsuarioIdComCache', proxy('http://localhost:8080', {
  proxyReqPathResolver: function (req) {    
    return '/metodos/consultaUsuarioIdComCache';
  },
  proxyReqOptDecorator: function (proxyReqOpts, srcReq) {
    return new Promise(function (resolve, reject) {
      proxyReqOpts.headers['accept-encoding'] = '';
      resolve(proxyReqOpts);
    })
  },
  userResDecorator: async function (proxyRes, proxyResData, userReq, userRes) {
    var data = proxyResData.toString('utf8');
    var jdata = JSON.parse(data);
    console.log('RECEBIDO: ' + JSON.stringify(jdata));
    return modificaCachePgProcessamento(jdata)
      .then(dataModificada => {
        console.log('ENVIADO: ' + JSON.stringify(dataModificada));
        return dataModificada
      })
  }
}));

app.use('/', proxy('http://localhost:8080'));


// app.use('/', proxy('http://localhost:810', {
//   userResDecorator: async function (proxyRes, proxyResData, userReq, userRes) {
//     var patt = /http:\/\/servidorCatracaIF\/cachepgprocessamento\/[0-9]*$/;
//     var modificar = new RegExp(patt).test(userReq.originalUrl.toLocaleLowerCase());

//     if (modificar) {
//       var data = JSON.parse(proxyResData.toString('utf8'));
//       console.log('RECEBIDO: ' + JSON.stringify(data));
//       return modificaCachePgProcessamento(data)
//         .then(dataModificada => {
//           console.log('ENVIADO: ' + JSON.stringify(dataModificada));
//           return dataModificada
//         })
//     } else {
// //      console.log(userReq.originalUrl + ' - SEM MODIFICACAO');
//       return proxyResData;
//     } 
//   }
// }));


app.listen(3000, function () {
  console.log('srvRNCliIF rodando na porta 3000');
});